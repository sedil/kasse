#ifndef MAINFRAME_H
#define MAINFRAME_H

#include <QWidget>

#include "viewfield.h"
#include "itemfield.h"
#include "numberfield.h"

class Control;

class MainFrame : public QWidget {
private:
    Q_OBJECT
    Control *_control;
    ViewField *_viewfield;
    ItemField *_itemfield;
    NumberField *_numberfield;
    QString _kassenzettel;

    QList<QPushButton*> _btnlist;
    unsigned short _amount;
    double _sum, _gotMoney;

    void initComponents();
    void initSignalAndSlots();
    void initLayout();
    void getItemMultiplier();
    unsigned short getArticleNumber();
    void setMoneyFromCustomer();
    void updateViewList();
public:
    explicit MainFrame(Control *control, QWidget *parent = nullptr);
    virtual ~MainFrame();
    void setChoosedItem(const QString &itemname);
    void enableCalculateButton();
    void setAmountofItem(unsigned short value);
    Control* getControler();
    ViewField* getViewField();
public slots:
    void calculateItems();
    void payment();
    void removeLastItemFromList();
    void finish();
    void getNumber();
    void searchArticle();
    void next();
};

#endif // MAINFRAME_H
