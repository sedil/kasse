#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QList>

#include "item.h"

class Calculator {
private:

public:
    explicit Calculator();
    virtual ~Calculator();

    void getTotalSum(const QList<Item*> &list, double &sum, double &netto);
    void calculate(const QList<Item*> &list, double &sum, double &gotMoney, double &netto, double &back, QList<QString> &zettel);
    QList<QString> finish(double &sum, double &netto, const QList<Item*> &selleditems, unsigned short customers);
};

#endif // CALCULATOR_H
