#ifndef ITEMFIELD_H
#define ITEMFIELD_H

#include <QWidget>
#include <QList>
#include <QPushButton>

class MainFrame;
class Controler;

class ItemField : public QWidget {
private:
    Q_OBJECT

    MainFrame *_mainframe;
    QList<QPushButton*> _btnlist;

    void initComponents();
    void initSignalAndSlots();
    void initLayout();
public:
    explicit ItemField(MainFrame *mainframe, QWidget *parent = nullptr);
    virtual ~ItemField();
signals:

public slots:
    void chooseItem01();
    void chooseItem02();
    void chooseItem03();
    void chooseItem04();
    void chooseItem05();
    void chooseItem06();
    void chooseItem07();
    void chooseItem08();
    void chooseItem09();
    void chooseItem10();
    void chooseItem11();
    void chooseItem12();
    void chooseItem13();
    void chooseItem14();
    void chooseItem15();
    void chooseItem16();
    void chooseItem17();
    void chooseItem18();
    void chooseItem19();
    void chooseItem20();
    void chooseItem21();
    void chooseItem22();
    void chooseItem23();
    void chooseItem24();
    void chooseItem25();
    void chooseItem26();
    void chooseItem27();
    void chooseItem28();
    void chooseItem29();
    void chooseItem30();
};

#endif // ITEMFIELD_H
