#include "numberfield.h"
#include "mainframe.h"

#include <QGridLayout>
#include <QFont>

NumberField::NumberField(MainFrame *mainframe, QWidget *parent) : QWidget(parent), _mainframe(mainframe) {
    initComponents();
    initSignalAndSlots();
    initLayout();
}

NumberField::~NumberField() {}

void NumberField::initComponents(){
    _btnlist.append(new QPushButton(tr("1")));
    _btnlist.append(new QPushButton(tr("2")));
    _btnlist.append(new QPushButton(tr("3")));
    _btnlist.append(new QPushButton(tr("4")));
    _btnlist.append(new QPushButton(tr("5")));
    _btnlist.append(new QPushButton(tr("6")));
    _btnlist.append(new QPushButton(tr("7")));
    _btnlist.append(new QPushButton(tr("8")));
    _btnlist.append(new QPushButton(tr("9")));
    _btnlist.append(new QPushButton(tr("0")));
    _btnlist.append(new QPushButton(tr("00")));
    _btnlist.append(new QPushButton(tr("C")));

    QFont font;
    font.setBold(true);
    font.setPointSize(32);
    for(unsigned short i = 0; i < _btnlist.size(); i++){
        _btnlist.at(i)->setFont(font);
    }
}

void NumberField::initSignalAndSlots(){
    connect(_btnlist.at(0), SIGNAL(clicked(bool)), this, SLOT(append_One()));
    connect(_btnlist.at(1), SIGNAL(clicked(bool)), this, SLOT(append_Two()));
    connect(_btnlist.at(2), SIGNAL(clicked(bool)), this, SLOT(append_Three()));
    connect(_btnlist.at(3), SIGNAL(clicked(bool)), this, SLOT(append_Four()));
    connect(_btnlist.at(4), SIGNAL(clicked(bool)), this, SLOT(append_Five()));
    connect(_btnlist.at(5), SIGNAL(clicked(bool)), this, SLOT(append_Six()));
    connect(_btnlist.at(6), SIGNAL(clicked(bool)), this, SLOT(append_Seven()));
    connect(_btnlist.at(7), SIGNAL(clicked(bool)), this, SLOT(append_Eight()));
    connect(_btnlist.at(8), SIGNAL(clicked(bool)), this, SLOT(append_Nine()));
    connect(_btnlist.at(9), SIGNAL(clicked(bool)), this, SLOT(append_Zero()));
    connect(_btnlist.at(10), SIGNAL(clicked(bool)), this, SLOT(append_DZero()));
    connect(_btnlist.at(11), SIGNAL(clicked(bool)), this, SLOT(append_Erase()));
}

void NumberField::initLayout(){
    QGridLayout *layout = new QGridLayout();
    layout->setSpacing(0);
    layout->addWidget(_btnlist.at(0), 0, 0);
    layout->addWidget(_btnlist.at(1), 0, 1);
    layout->addWidget(_btnlist.at(2), 0, 2);
    layout->addWidget(_btnlist.at(3), 1, 0);
    layout->addWidget(_btnlist.at(4), 1, 1);
    layout->addWidget(_btnlist.at(5), 1, 2);
    layout->addWidget(_btnlist.at(6), 2, 0);
    layout->addWidget(_btnlist.at(7), 2, 1);
    layout->addWidget(_btnlist.at(8), 2, 2);
    layout->addWidget(_btnlist.at(9), 3, 0);
    layout->addWidget(_btnlist.at(10), 3, 1);
    layout->addWidget(_btnlist.at(11), 3, 2);
    setLayout(layout);
    show();
}

unsigned short NumberField::getItemMultiplier(){
    unsigned short amount = _numberentry.toUShort();
    _numberentry.clear();
    return amount;
}

double NumberField::getMoneyFromCustomer(){
    double gotMoney;
    if ( _numberentry.size() > 2 ){
        QString format = _numberentry.insert(_numberentry.size() - 2, '.');
        gotMoney = format.toDouble();
    } else {
        QString format = _numberentry.insert(0, "0.");
        gotMoney = format.toDouble();
    }
    _numberentry.clear();
    return gotMoney;
}

/* SLOTS */
void NumberField::append_One(){
    _numberentry += _btnlist.at(0)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Two(){
    _numberentry += _btnlist.at(1)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Three(){
    _numberentry += _btnlist.at(2)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Four(){
    _numberentry += _btnlist.at(3)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Five(){
    _numberentry += _btnlist.at(4)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Six(){
    _numberentry += _btnlist.at(5)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Seven(){
    _numberentry += _btnlist.at(6)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Eight(){
    _numberentry += _btnlist.at(7)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Nine(){
    _numberentry += _btnlist.at(8)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Zero(){
    _numberentry += _btnlist.at(9)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_DZero(){
    _numberentry += _btnlist.at(10)->text();
    _mainframe->getViewField()->updateEntry(_numberentry);
}

void NumberField::append_Erase(){
    _numberentry.remove(_numberentry.size()-1, 1);
    _mainframe->getViewField()->updateEntry(_numberentry);
}
