#include "item.h"

Item::Item(const unsigned short &id, const QString &name, const double &price, const double &stax)
    : _id(id), _amount(0), _name(name), _price(price), _salestax(stax)
{ }

Item::Item(const Item &cpy)
    : _id(cpy.getID()), _amount(cpy.getQuantity()), _name(cpy.getName()), _price(cpy.getPrice()), _salestax(cpy.getSalesTax())
{ }

Item::~Item(){}

void Item::incQuantity(){
    _amount++;
}

void Item::decQuantity(){
    if( getQuantity() > 0){
        _amount--;
    }
}

unsigned short Item::getID() const {
    return _id;
}

unsigned short Item::getQuantity() const {
    return _amount;
}

QString Item::getName() const {
    return _name;
}

double Item::getPrice() const {
    return _price;
}

double Item::getSalesTax() const {
    return _salestax;
}

QString Item::getListRepresentation() const {
    QString priceformat = QString::number(getPrice());
    if( priceformat.at(priceformat.size() - 2) == '.' ){
        priceformat.append("0");
    }
    return QString::number(getQuantity()) + " x " + getName() + " " + priceformat;
}

QString Item::toString() const {
    return QString::number(getID()) + " " + QString::number(getQuantity()) + " " + getName() + " " +QString::number(getPrice()) + " " + QString::number(getSalesTax());
}
