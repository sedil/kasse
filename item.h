#ifndef ITEM_H
#define ITEM_H

#include <QString>

class Item {
private:
    unsigned short _id, _amount;
    QString _name;
    double _price, _salestax;
public:
    explicit Item(const unsigned short &id, const QString &name, const double &price, const double &stax);
    explicit Item(const Item &cpy);
    virtual ~Item();

    void incQuantity();
    void decQuantity();

    unsigned short getID() const;
    unsigned short getQuantity() const;
    QString getName() const;
    double getPrice() const;
    double getSalesTax() const;
    QString getListRepresentation() const;
    QString toString() const;
};

#endif // ITEM_H
