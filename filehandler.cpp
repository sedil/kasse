#include <QGridLayout>
#include <QMessageBox>
#include <QTextStream>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QList>

#include "filehandler.h"
#include "item.h"
#include "control.h"
#include "mainframe.h"

FileHandler::FileHandler(QWidget *parent) : QWidget(parent) {
    initComponents();
    initSignalAndSlot();
    initLayout();
}

FileHandler::~FileHandler(){}

void FileHandler::initComponents(){
    _artLabel = new QLabel("Artikellistenverzeichnis : ");
    _artPath = new QLineEdit("/home/sebastian/Dokumente/programming/cppCode/Kasse/articles.json");
    _artPath->setMinimumWidth(384);
    _zetLabel = new QLabel("Datei fuer den Kassenzettel : ");
    _zetPath = new QLineEdit("/home/sebastian/Schreibtisch/zettel");
    _zetPath->setMinimumWidth(384);
    _sumLabel = new QLabel("Datei fuer den Kassenbericht :");
    _sumPath = new QLineEdit("/home/sebastian/Schreibtisch/bericht");
    _sumPath->setMinimumWidth(384);
    _confirm = new QPushButton("OK");
}

void FileHandler::initSignalAndSlot(){
    connect(_confirm, SIGNAL(clicked(bool)), this, SLOT(examine()));
}

void FileHandler::initLayout(){
    QGridLayout *layout = new QGridLayout();
    layout->addWidget(_artLabel, 0, 0);
    layout->addWidget(_artPath, 0, 1);
    layout->addWidget(_zetLabel, 1, 0);
    layout->addWidget(_zetPath, 1, 1);
    layout->addWidget(_sumLabel, 2, 0);
    layout->addWidget(_sumPath, 2, 1);
    layout->addWidget(_confirm, 3, 0, 1, 2);
    setLayout(layout);
    setWindowModality(Qt::ApplicationModal);
    setWindowTitle(tr("Datei initialisieren[*]"));
    show();
}

QList<Item*> FileHandler::getArticleList(){
    QString json;
    QFile file;
    file.setFileName(_articlePath);
    bool openJson = file.open(QIODevice::ReadOnly | QIODevice::Text );
    if ( !openJson ){
        QMessageBox::information(this,"Fehler","Artikelliste konnte nicht geoeffnet werden (Fehlercode 1)");
    }
    json = file.readAll();
    file.close();

    QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8());
    QJsonObject obj = doc.object();
    QJsonArray entries = obj["articles"].toArray();

    QList<Item*> itemlist;
    for(int i = 0; i < entries.size(); i++){
        QJsonObject x = entries[i].toObject();
        unsigned short id = x["id"].toInt();
        QString name = x["name"].toString();
        double price = x["price"].toDouble();
        double stax = x["tax"].toDouble();
        itemlist.append(new Item(id,name,price,stax));
    }
    return itemlist;
}

void FileHandler::saveZettel(const QList<QString> &zettel){
    if( _zettel->open(QIODevice::Append | QIODevice::Text) ){
        QTextStream stream(_zettel);
        for(unsigned short i = 0; i < zettel.size(); i++){
            QString line = zettel.at(i);
            stream << line;
        }
        _zettel->flush();
        _zettel->close();
    }
}

QString FileHandler::finish(const QList<QString> &result){
    if( _summary->open(QIODevice::WriteOnly | QIODevice::Text) ){
        QTextStream stream(_summary);
        for(unsigned short i = 0; i < result.size(); i++){
            QString line = result.at(i);
            stream << line;
        }
        _summary->flush();
        _summary->close();
        delete _zettel;
        delete _summary;
        return QString("Kassenabschlussbericht wurde gespeichert");
    } else {
        return QString("(Fehlercode 2)");
    }
}

/* SLOTS */
void FileHandler::examine(){
    _articlePath = QString(_artPath->text());
    QString zetPath = _zetPath->text();
    QString sumPath = _sumPath->text();

    if ( _articlePath.isEmpty() | zetPath.isEmpty() | sumPath.isEmpty() ){
        QMessageBox::information(this,"Fehler","Mindestens eine Pfadangabe ist leer");
        return;
    } else {
        _zettel = new QFile(zetPath);
        _summary = new QFile(sumPath);

        bool one = false, two = false;
        if ( _zettel->open(QIODevice::WriteOnly | QIODevice::Text)){
            _zettel->close();
            one = true;
        }
        if ( _summary->open(QIODevice::WriteOnly | QIODevice::Text)){
            _summary->close();
            two = true;
        }

        if ( !(one & two) ){
            QMessageBox::information(this,"Fehler","Mindestens eine Pfadangabe ist ungueltig");
            delete _zettel;
            delete _summary;
            return;
        } else {
            Control *control = new Control(this);
            new MainFrame(control);
            hide();
        }
    }
}
