#ifndef CONTROL_H
#define CONTROL_H

#include <QList>
#include <QString>

#include "calculator.h"
#include "item.h"
#include "filehandler.h"

class Control {
private:
    FileHandler *_filehandler;
    Calculator *_calculator;
    QList<Item*> _items, _temp, _total;
    unsigned short _customercounter;
    double _actualSum;

    QString readFile(const QString &path);
    void initItemList(const QString &json);
public:
    explicit Control(FileHandler *filehandler);
    virtual ~Control();

    void initArticleList();
    void updateActualSum(double value, bool add);
    void addItem(const Item &item);
    void addItem(const Item &item, unsigned short amount);
    void removeItem(const Item &item);
    void removeItem(const Item &item, unsigned short amount);
    void clearList();
    void resetActualSum();

    void requestPayment(double &sum, double &netto);
    void calculate(double &sum, double &gotMoney, double &back, QList<QString> &zettel);

    double getActualSum();
    QString finish();
    QList<Item*> getItemList();
    QList<Item*> getCustomerItemList();

    FileHandler* getFileHandler();
};

#endif // CONTROL_H
