#ifndef NUMBERFIELD_H
#define NUMBERFIELD_H

#include <QWidget>
#include <QList>
#include <QPushButton>

class MainFrame;

class NumberField : public QWidget {
private:
    Q_OBJECT

    MainFrame *_mainframe;
    QList<QPushButton*> _btnlist;
    QString _numberentry;

    void initComponents();
    void initSignalAndSlots();
    void initLayout();
public:
    explicit NumberField(MainFrame *mainframe, QWidget *parent = nullptr);
    virtual ~NumberField();

    unsigned short getItemMultiplier();
    double getMoneyFromCustomer();
signals:

public slots:
    void append_One();
    void append_Two();
    void append_Three();
    void append_Four();
    void append_Five();
    void append_Six();
    void append_Seven();
    void append_Eight();
    void append_Nine();
    void append_Zero();
    void append_DZero();
    void append_Erase();
};

#endif // NUMBERFIELD_H
