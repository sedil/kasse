#include <QGridLayout>
#include <QGroupBox>
#include <QTextStream>
#include <QMessageBox>
#include <QFileDialog>

#include "mainframe.h"
#include "articleviewer.h"
#include "control.h"

MainFrame::MainFrame(Control *control, QWidget *parent)
    : QWidget(parent), _control(control) {
    initComponents();
    initSignalAndSlots();
    initLayout();
}

MainFrame::~MainFrame() { }

void MainFrame::initComponents(){
    _viewfield = new ViewField(this);
    _numberfield = new NumberField(this);
    _itemfield = new ItemField(this);

    _btnlist.append(new QPushButton(tr("Ende")));
    _btnlist.append(new QPushButton(tr("Zahlvorgang")));
    _btnlist.append(new QPushButton(tr("Entfernen")));
    _btnlist.append(new QPushButton(tr("Abschluss")));
    _btnlist.append(new QPushButton(tr("x")));
    _btnlist.append(new QPushButton(tr("Suche Artikel")));
    _btnlist.append(new QPushButton(tr("Naechster")));

    _btnlist.at(0)->setEnabled(false);
    _btnlist.at(1)->setEnabled(false);

    _amount = 0;

    QFont font;
    font.setPointSize(12);
    font.setBold(true);
    for(unsigned short i = 0; i < _btnlist.size(); i++){
        _btnlist.at(i)->setFont(font);
    }
}

void MainFrame::initSignalAndSlots(){
    connect(_btnlist.at(0), SIGNAL(clicked(bool)), this, SLOT(calculateItems()));
    connect(_btnlist.at(1), SIGNAL(clicked(bool)), this, SLOT(payment()));
    connect(_btnlist.at(2), SIGNAL(clicked(bool)), this, SLOT(removeLastItemFromList()));
    connect(_btnlist.at(3), SIGNAL(clicked(bool)), this, SLOT(finish()));
    connect(_btnlist.at(4), SIGNAL(clicked(bool)), this, SLOT(getNumber()));
    connect(_btnlist.at(5), SIGNAL(clicked(bool)), this, SLOT(searchArticle()));
    connect(_btnlist.at(6), SIGNAL(clicked(bool)), this, SLOT(next()));
}

void MainFrame::initLayout(){
    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(_viewfield);

    QGroupBox *group = new QGroupBox();
    QVBoxLayout *controlbtn = new QVBoxLayout();
    controlbtn->addWidget(_btnlist.at(0));
    controlbtn->addWidget(_btnlist.at(1));
    controlbtn->addWidget(_btnlist.at(2));
    controlbtn->addWidget(_btnlist.at(3));
    controlbtn->addWidget(_btnlist.at(4));
    controlbtn->addWidget(_btnlist.at(5));
    controlbtn->addWidget(_btnlist.at(6));

    QHBoxLayout *controllayout = new QHBoxLayout();
    controllayout->addWidget(_itemfield);
    controllayout->addWidget(_numberfield);
    controllayout->addLayout(controlbtn);
    group->setLayout(controllayout);

    layout->addWidget(group);
    setLayout(layout);
    setWindowTitle(tr("Registriermaschine[*]"));
    show();
}

void MainFrame::setAmountofItem(unsigned short value){
    _amount = value;
}

void MainFrame::enableCalculateButton(){
    if( !_control->getCustomerItemList().isEmpty() ){
        _btnlist.at(0)->setEnabled(true);
    } else {
        _btnlist.at(0)->setEnabled(false);
    }
}

void MainFrame::getItemMultiplier(){
    _amount = _numberfield->getItemMultiplier();
}

unsigned short MainFrame::getArticleNumber(){
    return _numberfield->getItemMultiplier();
}

void MainFrame::setMoneyFromCustomer(){
    _gotMoney = _numberfield->getMoneyFromCustomer();
}

void MainFrame::setChoosedItem(const QString &itemname){
    for(unsigned short i = 0; i < _control->getItemList().size(); i++){
        if( _control->getItemList().at(i)->getName() == itemname ){
            _control->addItem(*(_control->getItemList().at(i)), _amount);
            _amount = 0;
            break;
        }
    }
    enableCalculateButton();
    updateViewList();
}

Control* MainFrame::getControler(){
    return _control;
}

ViewField* MainFrame::getViewField(){
    return _viewfield;
}

void MainFrame::updateViewList(){
    _viewfield->clearList();
    for(unsigned short j = 0; j < _control->getCustomerItemList().size(); j++){
        _viewfield->addListElement(_control->getCustomerItemList().at(j)->getListRepresentation());
    }
    _viewfield->updateActualSum(QString::number(_control->getActualSum()));
}

/* SLOTS */
void MainFrame::calculateItems(){
    _btnlist.at(0)->setEnabled(false);
    _btnlist.at(1)->setEnabled(true);
    _btnlist.at(2)->setEnabled(false);
    double netto;
    _control->requestPayment(_sum, netto);
    _viewfield->updateActualSum(QString::number(_control->getActualSum()));
    _viewfield->updateEndSum(QString::number(_sum));
}

void MainFrame::payment(){
    double back;
    QList<QString> zettel;
    setMoneyFromCustomer();
    _viewfield->updateMoneyFromCustomer();
    if ( _gotMoney < _sum ){
        QMessageBox::warning(this, "Fehler", "Einnahme ist niedriger als die Summe", "");
        return;
    }
    _control->calculate(_sum,_gotMoney,back,zettel);
    _viewfield->updateBack(QString::number(back));
    _sum = 0;
    _viewfield->clearList();
    _control->clearList();
    zettel.append("Vielen Dank fuer Ihren Einkauf \n\n");

    _control->getFileHandler()->saveZettel(zettel);

    _btnlist.at(1)->setEnabled(false);
    _btnlist.at(2)->setEnabled(true);
}

void MainFrame::removeLastItemFromList(){
    if ( _control->getCustomerItemList().isEmpty() ){
        QMessageBox::warning(this, "Fehler", "Es kann nichts geloescht werden", "");
        return;
    }
    _control->removeItem(*(_control->getCustomerItemList().last()), _control->getCustomerItemList().last()->getQuantity()); // Richtiges Item von ItemField bekommen
    updateViewList();
    enableCalculateButton();
    _viewfield->updateActualSum(QString::number(_control->getActualSum()));
}

void MainFrame::finish(){
    QString msg = _control->finish();
    QMessageBox::information(this, "Kassenbericht speichern",msg, "");
}

void MainFrame::getNumber(){
    getItemMultiplier();
}

void MainFrame::searchArticle(){
    ArticleViewer *av = new ArticleViewer(this);
    av->addArticlesInList(_control->getItemList());
}

void MainFrame::next(){
    _sum = 0;
    _gotMoney = 0;
    _control->resetActualSum();
    _viewfield->clearLabels();
}
