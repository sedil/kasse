#include <QGridLayout>

#include "itemfield.h"
#include "mainframe.h"
#include "control.h"

ItemField::ItemField(MainFrame *mainframe, QWidget *parent) : QWidget(parent), _mainframe(mainframe) {
    initComponents();
    initSignalAndSlots();
    initLayout();
}

ItemField::~ItemField(){}

void ItemField::initComponents(){
    unsigned short places = 30, i;
    for(i = 0; i < _mainframe->getControler()->getItemList().size();i++){
        _btnlist.append(new QPushButton(_mainframe->getControler()->getItemList().at(i)->getName()));
        _btnlist.at(i)->setFixedSize(128,48);
        places--;
    }

    for(unsigned short p = 0; p < places; p++){
        _btnlist.append(new QPushButton(tr("")));
        _btnlist.at(i+p)->setFixedSize(128,48);
    }
}

void ItemField::initSignalAndSlots(){
    connect(_btnlist.at(0), SIGNAL(clicked(bool)), this, SLOT(chooseItem01()));
    connect(_btnlist.at(1), SIGNAL(clicked(bool)), this, SLOT(chooseItem02()));
    connect(_btnlist.at(2), SIGNAL(clicked(bool)), this, SLOT(chooseItem03()));
    connect(_btnlist.at(3), SIGNAL(clicked(bool)), this, SLOT(chooseItem04()));
    connect(_btnlist.at(4), SIGNAL(clicked(bool)), this, SLOT(chooseItem05()));
    connect(_btnlist.at(5), SIGNAL(clicked(bool)), this, SLOT(chooseItem06()));
    connect(_btnlist.at(6), SIGNAL(clicked(bool)), this, SLOT(chooseItem07()));
    connect(_btnlist.at(7), SIGNAL(clicked(bool)), this, SLOT(chooseItem08()));
    connect(_btnlist.at(8), SIGNAL(clicked(bool)), this, SLOT(chooseItem09()));
    connect(_btnlist.at(9), SIGNAL(clicked(bool)), this, SLOT(chooseItem10()));
    connect(_btnlist.at(10), SIGNAL(clicked(bool)), this, SLOT(chooseItem11()));
    connect(_btnlist.at(11), SIGNAL(clicked(bool)), this, SLOT(chooseItem12()));
    connect(_btnlist.at(12), SIGNAL(clicked(bool)), this, SLOT(chooseItem13()));
    connect(_btnlist.at(13), SIGNAL(clicked(bool)), this, SLOT(chooseItem14()));
    connect(_btnlist.at(14), SIGNAL(clicked(bool)), this, SLOT(chooseItem15()));
    connect(_btnlist.at(15), SIGNAL(clicked(bool)), this, SLOT(chooseItem16()));
    connect(_btnlist.at(16), SIGNAL(clicked(bool)), this, SLOT(chooseItem17()));
    connect(_btnlist.at(17), SIGNAL(clicked(bool)), this, SLOT(chooseItem18()));
    connect(_btnlist.at(18), SIGNAL(clicked(bool)), this, SLOT(chooseItem19()));
    connect(_btnlist.at(19), SIGNAL(clicked(bool)), this, SLOT(chooseItem20()));
    connect(_btnlist.at(20), SIGNAL(clicked(bool)), this, SLOT(chooseItem21()));
    connect(_btnlist.at(21), SIGNAL(clicked(bool)), this, SLOT(chooseItem22()));
    connect(_btnlist.at(22), SIGNAL(clicked(bool)), this, SLOT(chooseItem23()));
    connect(_btnlist.at(23), SIGNAL(clicked(bool)), this, SLOT(chooseItem24()));
    connect(_btnlist.at(24), SIGNAL(clicked(bool)), this, SLOT(chooseItem25()));
    connect(_btnlist.at(25), SIGNAL(clicked(bool)), this, SLOT(chooseItem26()));
    connect(_btnlist.at(26), SIGNAL(clicked(bool)), this, SLOT(chooseItem27()));
    connect(_btnlist.at(27), SIGNAL(clicked(bool)), this, SLOT(chooseItem28()));
    connect(_btnlist.at(28), SIGNAL(clicked(bool)), this, SLOT(chooseItem29()));
    connect(_btnlist.at(29), SIGNAL(clicked(bool)), this, SLOT(chooseItem30()));
}

void ItemField::initLayout(){
    QGridLayout *layout = new QGridLayout();
    layout->setHorizontalSpacing(0);
    layout->setVerticalSpacing(0);
    layout->addWidget(_btnlist.at(0), 0, 0);
    layout->addWidget(_btnlist.at(1), 0, 1);
    layout->addWidget(_btnlist.at(2), 0, 2);
    layout->addWidget(_btnlist.at(3), 0, 3);
    layout->addWidget(_btnlist.at(4), 0, 4);
    layout->addWidget(_btnlist.at(5), 0, 5);
    layout->addWidget(_btnlist.at(6), 1, 0);
    layout->addWidget(_btnlist.at(7), 1, 1);
    layout->addWidget(_btnlist.at(8), 1, 2);
    layout->addWidget(_btnlist.at(9), 1, 3);
    layout->addWidget(_btnlist.at(10), 1, 4);
    layout->addWidget(_btnlist.at(11), 1, 5);
    layout->addWidget(_btnlist.at(12), 2, 0);
    layout->addWidget(_btnlist.at(13), 2, 1);
    layout->addWidget(_btnlist.at(14), 2, 2);
    layout->addWidget(_btnlist.at(15), 2, 3);
    layout->addWidget(_btnlist.at(16), 2, 4);
    layout->addWidget(_btnlist.at(17), 2, 5);
    layout->addWidget(_btnlist.at(18), 3, 0);
    layout->addWidget(_btnlist.at(19), 3, 1);
    layout->addWidget(_btnlist.at(20), 3, 2);
    layout->addWidget(_btnlist.at(21), 3, 3);
    layout->addWidget(_btnlist.at(22), 3, 4);
    layout->addWidget(_btnlist.at(23), 3, 5);
    layout->addWidget(_btnlist.at(24), 4, 0);
    layout->addWidget(_btnlist.at(25), 4, 1);
    layout->addWidget(_btnlist.at(26), 4, 2);
    layout->addWidget(_btnlist.at(27), 4, 3);
    layout->addWidget(_btnlist.at(28), 4, 4);
    layout->addWidget(_btnlist.at(29), 4, 5);
    setLayout(layout);
    show();
}

/* SLOTS */
void ItemField::chooseItem01(){
    if ( !(_btnlist.at(0)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(0)->text());
    }
}

void ItemField::chooseItem02(){
    if ( !(_btnlist.at(1)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(1)->text());
    }
}

void ItemField::chooseItem03(){
    if ( !(_btnlist.at(2)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(2)->text());
    }
}

void ItemField::chooseItem04(){
    if ( !(_btnlist.at(3)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(3)->text());
    }
}

void ItemField::chooseItem05(){
    if ( !(_btnlist.at(4)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(4)->text());
    }
}

void ItemField::chooseItem06(){
    if ( !(_btnlist.at(5)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(5)->text());
    }
}

void ItemField::chooseItem07(){
    if ( !(_btnlist.at(6)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(6)->text());
    }
}

void ItemField::chooseItem08(){
    if ( !(_btnlist.at(7)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(7)->text());
    }
}

void ItemField::chooseItem09(){
    if ( !(_btnlist.at(8)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(8)->text());
    }
}

void ItemField::chooseItem10(){
    if ( !(_btnlist.at(9)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(9)->text());
    }
}

void ItemField::chooseItem11(){
    if ( !(_btnlist.at(10)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(10)->text());
    }
}

void ItemField::chooseItem12(){
    if ( !(_btnlist.at(11)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(11)->text());
    }
}

void ItemField::chooseItem13(){
    if ( !(_btnlist.at(12)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(12)->text());
    }
}

void ItemField::chooseItem14(){
    if ( !(_btnlist.at(13)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(13)->text());
    }
}

void ItemField::chooseItem15(){
    if ( !(_btnlist.at(14)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(14)->text());
    }
}

void ItemField::chooseItem16(){
    if ( !(_btnlist.at(15)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(15)->text());
    }
}

void ItemField::chooseItem17(){
    if ( !(_btnlist.at(16)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(16)->text());
    }
}

void ItemField::chooseItem18(){
    if ( !(_btnlist.at(17)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(17)->text());
    }
}

void ItemField::chooseItem19(){
    if ( !(_btnlist.at(18)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(18)->text());
    }
}

void ItemField::chooseItem20(){
    if ( !(_btnlist.at(19)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(19)->text());
    }
}

void ItemField::chooseItem21(){
    if ( !(_btnlist.at(20)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(20)->text());
    }
}

void ItemField::chooseItem22(){
    if ( !(_btnlist.at(21)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(21)->text());
    }
}

void ItemField::chooseItem23(){
    if ( !(_btnlist.at(22)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(22)->text());
    }
}

void ItemField::chooseItem24(){
    if ( !(_btnlist.at(23)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(23)->text());
    }
}

void ItemField::chooseItem25(){
    if ( !(_btnlist.at(24)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(24)->text());
    }
}

void ItemField::chooseItem26(){
    if ( !(_btnlist.at(25)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(25)->text());
    }
}

void ItemField::chooseItem27(){
    if ( !(_btnlist.at(26)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(26)->text());
    }
}

void ItemField::chooseItem28(){
    if ( !(_btnlist.at(27)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(27)->text());
    }
}

void ItemField::chooseItem29(){
    if ( !(_btnlist.at(28)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(28)->text());
    }
}

void ItemField::chooseItem30(){
    if ( !(_btnlist.at(29)->text()).isEmpty() ){
        _mainframe->setChoosedItem(_btnlist.at(29)->text());
    }
}
