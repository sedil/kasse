#include "articleviewer.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>

ArticleViewer::ArticleViewer(MainFrame *mainframe, QWidget *parent) : QWidget(parent), _mainframe(mainframe) {
    initComponents();
    initSignalsAndSlots();
    initLayout();
}

ArticleViewer::~ArticleViewer() { }

bool ArticleViewer::examineEntry(){
    QString entry = _amount->text();
    for(unsigned short i = 0; i < entry.size(); i++){
        if( !entry.at(i).isDigit() ){
            QMessageBox::warning(this, "Fehler", "Es duerfen nur Zahlen eingegeben werden", "");
            return false;
        }
    }
    unsigned short temp = entry.toUShort();

    if ( temp < 1 | temp > 99 ){
        QMessageBox::warning(this, "Fehler", "Es sind nur Werte zwischen 1 und 99 erlaubt", "");
        return false;
    }
    return true;
}

void ArticleViewer::initComponents(){
    _chooseBtn = new QPushButton(tr("Artikel waehlen"));
    _confirm = new QPushButton(tr("OK"));
    _abort = new QPushButton(tr("Zurueck"));
    _infobox = new QLabel();
    _amountLabel = new QLabel("Anzahl :");
    _amount = new QLineEdit();
}

void ArticleViewer::initSignalsAndSlots(){
    connect(_confirm, SIGNAL(clicked(bool)), this, SLOT(confirmEntry()));
    connect(_abort, SIGNAL(clicked(bool)), this, SLOT(back()));
    connect(_chooseBtn, SIGNAL(clicked(bool)), this, SLOT(chooseItem()));
}

void ArticleViewer::initLayout(){
    _articlelist = new QListWidget();
    _articlelist->setSelectionMode(QAbstractItemView::SingleSelection);
    QVBoxLayout *outer = new QVBoxLayout();
    outer->addWidget(_articlelist);
    QHBoxLayout *controlelements = new QHBoxLayout();
    controlelements->addWidget(_amountLabel);
    controlelements->addWidget(_amount);
    controlelements->addWidget(_chooseBtn);
    controlelements->addWidget(_confirm);
    controlelements->addWidget(_abort);
    outer->addWidget(_infobox);
    outer->addLayout(controlelements);
    setLayout(outer);
    setWindowTitle(tr("Artikel suchen[*]"));
    show();
}

void ArticleViewer::addArticlesInList(QList<Item*> articles){
    for(const Item* article : articles){
        _articlelist->addItem(article->getName());
    }
    _articlelist->setCurrentRow(0);
    _infobox->setText(_articlelist->currentItem()->text());
}

/* SLOTS */
void ArticleViewer::chooseItem(){
    QListWidgetItem *t = _articlelist->currentItem();
    _infobox->setText(t->text());
}

void ArticleViewer::confirmEntry(){
    if ( examineEntry() ) {
        QString amount = _amount->text();
        _mainframe->setAmountofItem(amount.toUShort());
        _mainframe->setChoosedItem(_infobox->text());
        close();
    }
}

void ArticleViewer::back() {
    close();
}
