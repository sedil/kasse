#include "calculator.h"

Calculator::Calculator() { }

Calculator::~Calculator() { }

void Calculator::getTotalSum(const QList<Item*> &list, double &sum, double &netto){
    sum = 0;
    netto = 0;
    for(unsigned short i = 0; i < list.size(); i++){
        sum += list.at(i)->getQuantity() * list.at(i)->getPrice();
        double mwst = (list.at(i)->getQuantity() * list.at(i)->getPrice()) / list.at(i)->getSalesTax();
        mwst = static_cast<double>(static_cast<int>(mwst * 100)) / 100;
        netto += (list.at(i)->getQuantity() * list.at(i)->getPrice()) - mwst;
    }
}

void Calculator::calculate(const QList<Item*> &list, double &sum, double &gotMoney, double &netto, double &back, QList<QString> &zettel){
    getTotalSum(list, sum, netto);
    back = gotMoney - sum;
    double mwst = sum - netto;

    for(unsigned short i = 0; i < list.size(); i++){
        zettel.append(QString::number(list.at(i)->getQuantity())+" "+list.at(i)->getName()+" "+QString::number(list.at(i)->getPrice())+" "+QString::number(list.at(i)->getSalesTax()) + "\n");
    }
    zettel.append("MwSt : " + QString::number(mwst) + "\n");
    zettel.append("Zu zahlen : " + QString::number(sum) + "\n");
    zettel.append("Gegeben : " + QString::number(gotMoney) + "\n");
    zettel.append("Zurueck : " + QString::number(back) + "\n");
}

QList<QString> Calculator::finish(double &totalsum, double &netto, const QList<Item*> &selleditems, unsigned short customers){
    getTotalSum(selleditems, totalsum, netto);

    QList<QString> result;
    double gewinn = totalsum - netto;
    result.append("Anzahl der Kunden : " + QString::number(customers) + "\n");
    result.append("Eingenommen : " + QString::number(totalsum) + "\n");
    result.append("Mehrwertsteuern : " + QString::number(netto) + "\n");
    result.append("Gewinn : " + QString::number(gewinn) + "\n");
    result.append("\n");
    for(unsigned short i = 0; i < selleditems.size(); i++){
        if ( selleditems.at(i)->getQuantity() > 0 ){
            result.append(selleditems.at(i)->toString() + "\n");
        }
    }
    return result;
}
