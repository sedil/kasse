#include "control.h"

Control::Control(FileHandler *fh) : _filehandler(fh), _customercounter(0), _actualSum(0.00) {
    _items = _filehandler->getArticleList();
    for(unsigned short i = 0; i <_items.size(); i++){
        _total.append(_items.at(i));
    }
}

Control::~Control(){ }

void Control::updateActualSum(double value, bool add){
    if ( add ) {
        _actualSum += value;
    } else {
        _actualSum -= value;
        if ( _actualSum < 0 ){
            resetActualSum();
        }
    }
}

void Control::resetActualSum(){
    _actualSum = 0;
}

void Control::addItem(const Item &item){
    for(unsigned short i = 0; i < _temp.size(); i++){
        if ( item.getID() == _temp.at(i)->getID() ){
            _temp.operator[](i)->incQuantity();
            return;
        }
    }
    _temp.append(new Item(item.getID(),item.getName(),item.getPrice(),item.getSalesTax()));
    _temp.last()->incQuantity();
}

void Control::addItem(const Item &item, unsigned short amount){
    for(unsigned short i = 0; i < amount; i++){
        addItem(item);
    }
    updateActualSum(static_cast<double>(item.getPrice() * amount), true);
}

void Control::removeItem(const Item &item){
    for(unsigned short i = 0; i < _temp.size(); i++){
        if( item.getID() == _temp.at(i)->getID() ){
            if ( _temp.at(i)->getQuantity() > 1 ){
                _temp.operator[](i)->decQuantity();
                return;
            } else {
                delete _temp.operator[](i);
                _temp.removeAt(i);
            }
        }
    }
}

void Control::removeItem(const Item &item, unsigned short amount){
    for(unsigned short i = 0; i < amount; i++){
        removeItem(item);
    }
    updateActualSum(static_cast<double>(item.getPrice() * amount), false);
}

void Control::clearList(){
    _temp.clear();
}

void Control::requestPayment(double &sum, double &netto){
    _calculator->getTotalSum(_temp,sum,netto);
}

void Control::calculate(double &sum, double &gotMoney, double &back, QList<QString> &zettel){
    double netto;

    Calculator *calculator = new Calculator();
    calculator->calculate(_temp,sum,gotMoney,netto,back,zettel);
    delete calculator;

    for(unsigned short i = 0; i < _temp.size(); i++){
        for(unsigned short j = 0; j < _total.size(); j++){
            if( _temp.at(i)->getID() == _total.at(j)->getID() ){
                for(unsigned short k = 0; k < _temp.at(i)->getQuantity(); k++){
                    _total.operator[](j)->incQuantity();
                }
                break;
            }
        }
    }
    _customercounter++;
}

QString Control::finish(){
    double sum, netto;

    Calculator *calculator = new Calculator();
    QList<QString> result = calculator->finish(sum, netto, _total, _customercounter);
    delete calculator;
    return _filehandler->finish(result);
}

double Control::getActualSum() {
    return _actualSum;
}

QList<Item*> Control::getItemList() {
    return _items;
}

QList<Item*> Control::getCustomerItemList() {
    return _temp;
}

FileHandler* Control::getFileHandler(){
    return _filehandler;
}
