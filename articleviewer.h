#ifndef ARTICLEVIEWER_H
#define ARTICLEVIEWER_H

#include <QWidget>
#include <QLineEdit>

#include "mainframe.h"
#include "item.h"

class ArticleViewer : public QWidget {
private:
    Q_OBJECT

    MainFrame *_mainframe;
    QListWidget *_articlelist;
    QPushButton *_confirm, *_abort, *_chooseBtn;
    QLabel *_infobox;
    QLabel *_amountLabel;
    QLineEdit *_amount;

    bool examineEntry();
public:
    explicit ArticleViewer(MainFrame *mainframe, QWidget *parent = nullptr);
    virtual ~ArticleViewer();

    void initComponents();
    void initSignalsAndSlots();
    void initLayout();

    void addArticlesInList(QList<Item*> articles);
signals:

public slots:
    void chooseItem();
    void confirmEntry();
    void back();
};

#endif // ARTICLEVIEWER_H
